﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPI.Models
{
    public class UserCurrency
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public float USD { get; set; }
        public float EUR { get; set; }
        public float RUB { get; set; }
        public float UAH { get; set; }
        public float CZK { get; set; }
    }
}
