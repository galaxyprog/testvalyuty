﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestAPI.DataAccess;
using RestAPI.Models;
using RestAPI.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;

namespace RestAPI.Controllers
{
    [ApiController]
    public class ValuesController : ControllerBase
    {
        UserContext db;

        public GetValueAPIService GetValueAPIService { get; }

        public ValuesController(UserContext context, GetValueAPIService getValueAPIService)
        {
            GetValueAPIService = getValueAPIService;
            db = context;
            //Заполняем БД начальными данными, если она пуста
            if (!db.UserCurrencies.Any())
            {
                db.UserCurrencies.Add(new UserCurrency { UserId = "user1", RUB = 13.22f, CZK = 135.40f, EUR = 100.40f, USD = 150.60f, UAH = 200.34f });
                db.UserCurrencies.Add(new UserCurrency { UserId = "user2", RUB = 3232.12f, CZK = 13335.40f, EUR = 1111.40f, USD = 1990.60f, UAH = 0.00f });
                db.SaveChanges();
            }
        }
        /// <summary>
        /// Отображает идентификаторы зарегистрированных в системе пользователей
        /// </summary>
        /// <returns></returns>
        [Route("/api/[controller]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> Get()
        {
            return await db.UserCurrencies.Select(u => u.UserId).ToListAsync();
        }
        /// <summary>
        /// С помощью метода Get с указанным в аргументе идентификатором пользователя можно получить текущее состояние своего кошелька
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("/api/[controller]/{userId}")]
        [HttpGet]
        public async Task<ActionResult<UserCurrency>> Get(string userId)
        {
            UserCurrency userCurrency = await db.UserCurrencies.SingleOrDefaultAsync(uc => uc.UserId == userId);
            if (userCurrency is null)
            {
                return NotFound();
            }
            
            return userCurrency;
        }

        /// <summary>
        /// Метод Change позволяет одновременно и пополнить кошелек, и снять деньги в одной из валют в зависимости от того является ли число нужной валюты
        /// в экземляре класса UserCurrency положительным или же отрицательным.
        /// В метод Change поступает экземпляр класса UserCurrency с изменениями в необходимых полях, затем сам метод их обрабатывает и 
        /// сохраняет в БД данные текущего пользователя с учетом изменений
        /// </summary>
        /// <param name="userCurrency"></param>
        /// <returns></returns>
        [Route("/api/[controller]/change")]
        [HttpPatch]
        public async Task<ActionResult> Change([FromBody] UserCurrency userCurrency)
        {
            UserCurrency userDB = db.UserCurrencies.Where(u => u.UserId == userCurrency.UserId).SingleOrDefault();
            if (userDB is null)
            {
                return BadRequest();                
            }
            userDB.USD += userCurrency.USD;
            userDB.EUR += userCurrency.EUR;
            userDB.CZK += userCurrency.CZK;
            userDB.RUB += userCurrency.RUB;
            userDB.UAH += userCurrency.UAH;

            //Валидация кошелька
            var properties = typeof(UserCurrency).GetProperties().Skip(2);
            foreach (var prop in properties)
            {
                if ((float)typeof(UserCurrency).GetProperty(prop.Name).GetValue(userDB) < 0f)
                {
                    return BadRequest("Недостаточно денег на кошельке. Укажите корректную сумму");
                }
            }

            await db.SaveChangesAsync();

            return Ok(userDB);
        }

        /// <summary>
        /// С помощью метода Transfer осуществляется возможность конвертировать из одной валюты в другую для определенного пользователя.
        /// Формат входной строки transferData: "[ИдентификаторПользователя]->[СуммаКонвертации] [Валюта1]->[Валюта2]"
        /// Пример: "user1->111 USD->EUR"
        /// </summary>
        /// <param name="transferData"></param>
        /// <returns></returns>
        [Route("/api/[controller]/transfer")]
        [HttpPatch]
        public async Task<ActionResult> Transfer([FromBody]string transferData)
        {
            string[] chunks = transferData.Split("->");
            string userId = chunks[0];
            float totalTransfer = float.Parse(chunks[1].Split(" ")[0], CultureInfo.InvariantCulture.NumberFormat);
            string currencyTransfer = chunks[1].Split(" ")[1];
            string currencyToTransfer = chunks[2];

            UserCurrency userCurrency = await db.UserCurrencies.SingleOrDefaultAsync(uc => uc.UserId == userId);
            //Проверка правильности ввода айди пользователя
            if (userCurrency is null)
            {
                return BadRequest("Пользователь не найден");
            }
            
            PropertyInfo propertyFrom = typeof(UserCurrency).GetProperty(currencyTransfer);
            PropertyInfo propertyTo = typeof(UserCurrency).GetProperty(currencyToTransfer);

            //Извлечение текущих состояний кошелька
            float currentStateFrom = (float)propertyFrom.GetValue(userCurrency);
            float currentStateTo = (float)propertyTo.GetValue(userCurrency);

            float currentAPIFrom;
            float currentAPITo;

            //Проверка баланса в кошельке
            if (currentStateFrom < totalTransfer)
            {
                return BadRequest("Недостаточно денег в кошельке. Укажите корректную сумму");
            }
            //Подгружаем данные с АПИ банка
            JsonElement currencyAPI = await GetValueAPIService.Load();
            
            propertyFrom.SetValue(userCurrency, currentStateFrom - totalTransfer);

            //Обработка валюты перевода из рублей, либо в рубли
            if (currencyTransfer.Equals("RUB"))
            {
                currentAPITo = float.Parse(currencyAPI.GetProperty(currencyToTransfer).GetProperty("Value").ToString(), CultureInfo.InvariantCulture.NumberFormat);
                propertyTo.SetValue(userCurrency, currentStateTo + totalTransfer / currentAPITo);
                await db.SaveChangesAsync();
                return Ok(userCurrency);
            }
            if (currencyToTransfer.Equals("RUB"))
            {
                currentAPIFrom = float.Parse(currencyAPI.GetProperty(currencyTransfer).GetProperty("Value").ToString(), CultureInfo.InvariantCulture.NumberFormat);
                propertyTo.SetValue(userCurrency, currentStateTo + totalTransfer * currentAPIFrom);
                await db.SaveChangesAsync();
                return Ok(userCurrency);
            }

            //Все остальные случаи
            currentAPIFrom = float.Parse(currencyAPI.GetProperty(currencyTransfer).GetProperty("Value").ToString(), CultureInfo.InvariantCulture.NumberFormat);
            currentAPITo = float.Parse(currencyAPI.GetProperty(currencyToTransfer).GetProperty("Value").ToString(), CultureInfo.InvariantCulture.NumberFormat);

            propertyTo.SetValue(userCurrency, currentStateTo + totalTransfer * (currentAPIFrom / currentAPITo), null);

            //Сохраняем изменения в БД
            await db.SaveChangesAsync();
            return Ok(userCurrency);


        }

    }
}
