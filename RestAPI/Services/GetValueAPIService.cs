﻿using Newtonsoft.Json;
using RestAPI.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using System.Xml;

namespace RestAPI.Services
{
    public class GetValueAPIService
    {
        private static string url = @"https://www.cbr-xml-daily.ru/daily_json.js";

        public static HttpClient ApiClient { get; set; }

        public static async Task<JsonElement> Load()
        {
            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    var stream = await response.Content.ReadAsStreamAsync();

                    var json = JsonDocument.Parse(stream);

                    var valutes = json.RootElement.GetProperty("Valute");

                    return valutes;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }           
        }
    }
}
